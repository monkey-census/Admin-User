const environment = require('../environment');

module.exports = function (app, User) {
    app.post(environment.userAPI, (req, res) => {
        const user = req.body;
        User(user).save((err, doc) => {
            if (err) {
                res.status('500').send(err);
                return;
            } else {
                if (doc.password) doc.password = undefined;
                res.send(doc);
            }
        });
    });
};