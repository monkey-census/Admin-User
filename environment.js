const defaults = {
    environment: 'development',
    port: '8000',

    signingSecret: 'sample-secret',
    tokenExpiry: '1h',
    tokenRenew: '30m',

    authenticateAPI: '/authenticate',
    validateAPI: '/validate',
    userAPI: '/users',

    mongoUrl: 'mongodb://localhost:27017',
    mongoDbName: 'development'
};

const environmentVariables = {
    environment: process.env.ENVIRONMENT,
    port: process.env.PORT,
    signingSecret: process.env.SIGNING_SECRET,
    mongoUrl: process.env.MONGO_URL,
    mongoDbName: process.env.MONGO_DATABASE,
};

Object.keys(environmentVariables).forEach(key => {
    if (environmentVariables[key] === undefined) {
        delete environmentVariables[key];
    }
});

module.exports = Object.assign({}, defaults, environmentVariables);