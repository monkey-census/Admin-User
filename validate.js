const environment = require('./environment');
const jwt = require('jsonwebtoken');

module.exports = function (token) {
    // Try catch incase there is an error decoding the jsonwebtoken, which probably
    // means its invalid or expired
    try {
        // verify the token, doesn't throw an error if it has expired
        if (jwt.verify(token, environment.signingSecret)) {
            // check if the token will expire within the token renew time
            if (Date(jwt.decode(token)['exp']) - Date(environment.tokenRenew) > Date.now()) {
                //if the token expires soon, generate a new token and return it
                const user = jwt.decode(token);
                delete user.iat; // delete the JWT fields, otherwise we might overwrite them
                delete user.exp;
                const token = jwt.sign(user, environment.signingSecret, {
                    expiresIn: environment.tokenExpiry
                });
                return token;
            } else {
                // the token is valid and doesn't need to be renewed
                return true;
            }
        } else {
            // token is invalid or expired
            return false;
        }
        // If we catch an error, something went wrong, so assume the token is invalid
    } catch (e) {
        return false;
    }
}