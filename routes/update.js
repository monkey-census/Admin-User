const ObjectID = require('mongodb').ObjectID;
const environment = require('../environment');
const _ = require('lodash');

module.exports = function (app, User) {
    app.put(environment.userAPI + '/:id', (req, res) => {
        const detail = {
            '_id': new ObjectID(req.params.id)
        };
        // just in case someone tries to save to the hash field
        const user = _.omit(req.body, 'hash');
        User.findOne(detail).exec((err, doc) => {
            if (err) {
                res.status('500').send(err);
                return;
            } else {
                Object.assign(doc, user);
                doc.save((err, savedUser) => {
                    if (err) {
                        res.status('500').send(err);
                        return;
                    }
                    res.send(savedUser);
                })
            }
        });
    });
};