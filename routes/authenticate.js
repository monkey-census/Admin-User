const environment = require('../environment');
const jwt = require('jsonwebtoken');
const validate = require('../validate');

module.exports = function (app, User) {
    // This part sets up the POST http request for authenticating a user
    // In order to keep the API endpoint url in a convenient place, I have added
    // a reference to the environment.
    app.post(environment.authenticateAPI, (req, res) => {
        // What we are searching for in MongoDB
        // the reason I have both userName and username is because I can never
        // remember which casing format I use, so I decided to use both, just in case.
        const detail = {
            userName: req.body.userName || req.body.username
        };

        // Here is where we actually search for the user
        User.findOne(detail).exec((err, user) => {
            // if there is an error or no user, then the user we were searching
            // for is unauthorized
            if (err || !user) {
                res.status('401').send();
            } else {
                // If we find the user, then check if the password matches
                user.authenticate(req.body.password).then(auth => {
                    if (auth) {
                        // If the user password matches, return a JWT token
                        res.json({
                            token: user.toJWT()
                        });
                    } else {
                        // Otherwise the user is not authorized
                        res.status('401').send();
                    }
                });
            }
        });
    });

    // Here is where we setup the token validation, this endpoint will be used by other
    // microservices to validate user tokens, this allows us to keep the token verification
    // in one place, and not shared amoung the other microservices
    app.post(environment.validateAPI, (req, res) => {
        // This is the function that we setup earlier, to verify tokens
        const verify = validate(req.body.token);
        if (verify) {
            if (verify === true) {
                // if the token is valid, return a 200 status
                res.status(200).send();
            } else {
                // if the token is renewed, return the renewed token
                res.json({
                    token: verify
                });
            }
        } else {
            // if the token fails, return unauthorized
            res.status('401').send();
        }
    });
};