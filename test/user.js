process.env.ENVIRONMENT = 'test';
const environment = require('../environment');

const jwt = require('jsonwebtoken');

// Get the mongodb dependencies
const mongoUrl = environment.mongoUrl + '/' + environment.mongoDbName;
const mongoose = require('mongoose');
const User = require('../schema/user');

// Setup chai, including the http extenstion
const chai = require('chai');
const chaiHttp = require('chai-http');
const should = chai.should();
chai.use(chaiHttp);

// Get the Express App to use with chaiHttp
const server = require('../server');
// Create a fake login token so we can get past the validated check
const fakeToken = jwt.sign({
    username: 'fakeuser'
}, environment.signingSecret);

let database = null;
describe('User Authentication Tests', () => {
    // Before each test, connect to MongoDb and delete any users that might be hanging around in the database
    // from previous tests, not exactly elegant, but it gets the job done
    beforeEach((done) => {
        mongoose.connect(mongoUrl, {
            useNewUrlParser: true
        });
        mongoose.connection.once('connected', () => {
            User.remove({}, () => done());
        });
    })

    afterEach(() => {
        mongoose.disconnect();
    })

    describe('User CRUD', () => {
        it('should GET a blank array of users', (done) => {
            chai.request(server)
                .get('/users')
                // make sure we add the fake token we created earlier to the request
                .set({
                    'authorization': 'Bearer ' + fakeToken
                })
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                });
        });

        it('should CREATE a new User and GET it in the list', (done) => {
            // we want to make multiple http calls, this is how you do that
            const requester = chai.request(server).keepOpen();

            requester.post('/users')
                // make sure we add the fake token we created earlier to the request
                .set({
                    'authorization': 'Bearer ' + fakeToken
                })
                .send({
                    userName: 'testuser',
                    password: 'abcd1234'
                })
                .then((res) => {
                    res.should.have.status(200);
                    res.body.should.have.property('id');
                }).then(() => {
                    return requester.get('/users')
                        // make sure we add the fake token we created earlier to the request
                        .set({
                            'authorization': 'Bearer ' + fakeToken
                        })
                }).then((res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(1);
                }).then(() => {
                    requester.close();
                    done();
                });
        });
    });

    describe('Authenticate', () => {

    });

    describe('Token Validate', () => {

    });
});