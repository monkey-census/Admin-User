const ObjectID = require('mongodb').ObjectID;
const environment = require('../environment');


module.exports = function (app, User) {
    app.delete(environment.userAPI + '/:id', (req, res) => {
        const detail = {
            '_id': new ObjectID(req.params.id)
        };
        User.remove(detail, (err, item) => {
            if (err) {
                res.status('500').send({
                    error: err
                });
            } else {
                res.send('user ' + req.params.id + ' deleted!');
            }
        });
    });
};