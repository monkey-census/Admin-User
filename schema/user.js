const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const environment = require('../environment');
const jwt = require('jsonwebtoken');
const _ = require('lodash');

// here we are defining the structure of a user, when saving to MongoDB
// Mongoose adds this functionality on top of MongoDB
var userSchema = mongoose.Schema({
    userName: {
        type: String,
        required: true,
        unique: true
    },
    hash: {
        type: String,
        required: true
    },
    email: String
});

// This allows us to have a virtual field on a User (when creating or updating)
// that will allow us to populate the password hash field with a one way hashed password
userSchema.virtual('password').set(function (value) {
    this.hash = bcrypt.hashSync(value, 10);
});

// This is a function that will exist on the model to help us check that the password
// matches what is expected using bcrypt to compare
userSchema.methods.authenticate = function (password) {
    return bcrypt.compare(password, this.hash);
}

// this function returns a jsonwebtoken with only the fields we care about
// if we add roles to a user later, we should include the roles field below (in the pick)
userSchema.methods.toJWT = function () {
    return jwt.sign(_.pick(this, 'userName', 'email'), environment.signingSecret, {
        expiresIn: environment.tokenExpiry
    });
}

// This transforms the model into an object, so that we can remove unnecessary fields
// for example, hash should never be returned from the microservice, this ensures it doesn't
userSchema.set('toJSON', {
    getters: true,
    transform: (doc, ret, options) => {
        ret.hash = undefined;
        ret._id = undefined;
        ret.__v = undefined;
        return ret;
    }
})

// Turning the schema into a model
var User = mongoose.model('User', userSchema);

module.exports = User;