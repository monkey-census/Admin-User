const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const colors = require('colors');
const environment = require('./environment');
const mongoose = require('mongoose');
const UserModel = require('./schema/user');

const app = express();

if (environment.environment !== 'production') {
    app.use(morgan('dev'));
}

// Looks for http requests with a JSON body, converts it to its object representation
app.use(bodyParser.json());

// This represents our connection to mongoDB through mongoose
var db = mongoose.connection;

// This logs to the console when mongoose is attempting to connect to mongoDB
db.on('connecting', function () {
    console.log(colors.yellow('connecting to MongoDB...'));
});
// This exits the process if there is an error when trying to connect to mongoDB
db.on('error', function (error) {
    console.error(colors.red('Error in MongoDb connection: ' + error));
    mongoose.disconnect();
    process.exit(1);
});
// When connected, log it to the console
db.on('connected', function () {
    console.log(colors.blue('connected to mongodb'));
});
// If mongoDB disconnects, try to reconnect
db.on('disconnected', function () {
    console.log(colors.red('MongoDB disconnected!'));
    mongoose.connect(environment.mongoUrl + '/' + environment.mongoDbName, {
        auto_reconnect: true,
        useNewUrlParser: true
    });
});

// Connect the first time to mongoDB, with auto reconnect
mongoose.connect(environment.mongoUrl + '/' + environment.mongoDbName, {
    auto_reconnect: true,
    useNewUrlParser: true
});

// check if the server is started with the expected commands, and has a subcommand
if (process.argv.length > 2 && process.argv[0].includes('node') && process.argv[1].includes('server.js')) {
    // get the subcommand
    const subCommand = process.argv[2];
    // get the rest of the arguements, if there are any
    const rest = process.argv.length > 3 ? process.argv.splice(3) : [];
    switch (subCommand) {
        // subcommand we are interested in
        case 'init':
            // search for an admin user
            UserModel.findOne({
                userName: 'admin'
            }, (err, user) => {
                if (err) {
                    console.log(err);
                    exit(1);
                }
                if (user === null) {
                    // If there is no admin user, create one
                    const newUser = UserModel({
                        userName: 'admin',
                        password: 'admin'
                    });
                    newUser.save((err, user) => {
                        if (err) {
                            console.log(err);
                            process.exit(1);
                        }
                        console.log(colors.green('admin user added to db'));
                        process.exit(0);
                    })
                } else {
                    // If the user exists, exit normally
                    console.log(colors.green('admin user already exists'));
                    process.exit(0);
                }
            });
            break;
    }
} else {
    // if there is no subcommand, start the http server normally
    // pass the express app and the User Model to the routes so they can use them
    require('./routes')(app, UserModel);
    app.listen(environment.port, () => {
        console.log(colors.blue('listening on port ' + environment.port));
    });
}

module.exports = app;