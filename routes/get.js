const ObjectID = require('mongodb').ObjectID;
const environment = require('../environment');


module.exports = function (app, User) {
    app.get(environment.userAPI + '/:id', (req, res) => {
        const detail = {
            '_id': new ObjectID(req.params.id)
        };
        User.findOne(detail, (err, item) => {
            if (err) {
                res.status('500').send({
                    error: err
                });
            } else {
                delete item.password;
                res.send(item);
            }
        });
    });
};