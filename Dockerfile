FROM node:boron

ENV PORT=80 MONGO_URL="mongodb://mongo:27017"

COPY ./dist /server

WORKDIR /server

RUN npm install

CMD [ "node", "server.js" ]