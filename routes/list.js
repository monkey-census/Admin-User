const ObjectID = require('mongodb').ObjectID;
const environment = require('../environment');


module.exports = function (app, User) {
    app.get(environment.userAPI, (req, res) => {
        User.find({}).exec((err, items) => {
            if (err) {
                res.status('500').send({
                    error: err
                });
            } else {
                res.send(items);
            }
        });
    });
};