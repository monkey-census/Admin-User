const environment = require('../environment');

const authenticate = require('./authenticate');
const create = require('./create');
const remove = require('./delete');
const get = require('./get');
const list = require('./list');
const update = require('./update');

const validate = require('../validate');

module.exports = function (app, UserModel) {
    // Add authenticate routes to the Http Server
    authenticate(app, UserModel);

    // check the user token is authorized for access to the user resource routes
    app.use(environment.userAPI, function (req, res, next) {
        // Check that a token has been attached to the request
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            // Handle token presented as a Bearer token in the Authorization header
            const token = req.headers.authorization.split(' ')[1];
            // Validate the token with our handy function
            const result = validate(token);

            if (result) {
                if (result === true) {
                    // if the token is valid passthrough the request
                    next();
                } else {
                    // if the token has been renewed, attach the new token to the response headers
                    // and passthrough the request
                    res.headers.authorization = 'Bearer ' + result;
                    next();
                }
            } else {
                // if the token is invalid, reject the request
                return res.status('401').send({
                    error: 'Expired or Invalid Authorization'
                });
            }
        } else {
            // if there is no token reject the request
            return res.status('401').send({
                error: 'No Authorization header!'
            });
        }
    });

    create(app, UserModel);
    remove(app, UserModel);
    get(app, UserModel);
    list(app, UserModel);
    update(app, UserModel);
};